# Adventures of Desil ( 2D RPG )

This was my thesis work for Eötvös Loránd University in 2017.
This game is a 2D RPG game, where you take the role of a young adult named Desil who wishes to be a great hero one day.
However, as in every tale, first he has to master the art of magic and combat. 
The goal of the game is to finish every main and side quests and in the meantime gather experience points and become stronger.

## The program

The game is a cross platform ( Linux and Windows ) x64 application. Supports multiple resolutions and graphics presets.
The game was written with the Godot(v2.1) engine. The main language was GD-Script ( very similar to Phyton ) most of the
game logic was written with this. Additionally I used my own C++ framework for handling the entity attributes and 
items. ( A newer version of this framework can be found in my repositories named "Reikon-Framework". )
Since the two languages could not communicate directly I've created a custom C++ module that I compiled into the engine
that created a "bridge" between the two systems.


### Menu
![](pictures/menu.png)
### Loading Screen
![](pictures/loading.png)
### Ingame in the town
![](pictures/townsquare.png)
### Map window
![](pictures/map.png)
### Quest window
![](pictures/journal.png)
### Attributes window
![](pictures/attributes.png)
### Dialog system
![](pictures/dialogsystem.png)
### Battle system
![](pictures/battle.png)